"use strict";

// Put your DOMContentLoaded event listener here first.
function setup(e) {
  console.log("hello world!");
  let gameBoard = document.getElementById("gameboard");
  let currentPlayer = "X";
  let winningmessage = document.getElementById("winningmsg");
  let currentPlayerMessage = document.getElementById("curplayer");
  const NewGame = document.getElementById("reset");
  
  let winningplayer = document.getElementById("winningplayer");
  const tdarray = document.querySelectorAll("td");
  const positions = [
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
    false,
  ];
  /*
   * function to play the game
   */
  function Game(e) {
    let clickedBox = e.target;

    //CHECKING IF CLICKED BOX IS OCCUPIED BY A SQUARE OR A O
    if (
      clickedBox.classList.contains("xsquare") ||
      clickedBox.classList.contains("osquare")
    ) {
      return;
    }

    //if winning message is shown on screen end event listener
    if (winningmessage.style.visibility === "visible") {
      return;
    }
    if (currentPlayer === "X") {
      if (clickedBox.textContent === "") {
        clickedBox.textContent = currentPlayer;
        clickedBox.classList.add("xsquare");
        currentPlayer = "O";
        currentPlayerMessage.textContent = `${currentPlayer}`;
      }
    } else {
      if (clickedBox.textContent === "") {
        clickedBox.textContent = currentPlayer;
        clickedBox.classList.add("osquare");
        currentPlayer = "X";
        currentPlayerMessage.textContent = `${currentPlayer}`;
      }
    }

    for (let i = 0; i < 9; i++) {
      if (tdarray[i].textContent != "") {
        positions[i] = tdarray[i].textContent;
        console.log(...positions);
      }
    }

    let results = checkBoard(...positions);
    if (results) {
      showWinningMessage(results);
    }
  }
  function showWinningMessage(winner) {
    winningplayer.textContent = winner;
    winningmessage.style.visibility = "visible";
  }

  function newGame(e) {
    for (let i = 0; i < positions.length; i++) {
      positions[i] = false;
      tdarray[i].textContent="";
      tdarray[i].classList.remove("xsquare");
      tdarray[i].classList.remove("osquare");
      
    }
    
    winningmessage.style.visibility = "hidden";
  }
  
  gameBoard.addEventListener("click", Game);
  NewGame.addEventListener("click", newGame);
}

document.addEventListener("DOMContentLoaded", setup);
